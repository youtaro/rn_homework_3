export const _getFanColor = (colorCheck) => {
  if (colorCheck === 'black') {
    return require('./../assets/fan-black.png');
  } else if (colorCheck === 'red') {
    return require('./../assets/fan-red.png');
  } else if (colorCheck === 'blue') {
    return require('./../assets/fan-blue.png');
  }
};

// export const _getFanStand = (colorCheck) => {
//   if (colorCheck === 'black') {
//     return require('./../assets/newStand.png');
//   } else if (colorCheck === 'red') {
//     return require('./../assets/stand-red.png');
//   } else if (colorCheck === 'blue') {
//     return require('./../assets/stand-blue.png');
//   }
// };
