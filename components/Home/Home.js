import {Body, Card, CardItem, H3, Icon, Right, Thumbnail} from 'native-base';
import React, {Component} from 'react';
import {
  Text,
  StyleSheet,
  View,
  ScrollView,
  Image,
  FlatList,
} from 'react-native';
import {homeData} from '../../Data/HomeData';

export default class Home extends Component {
  render() {
    return (
      <FlatList
        keyExtractor={(i) => i.id.toString()}
        data={homeData}
        renderItem={({item, i}) => (
          <Card>
            <CardItem>
              <Image
                style={{height: 100, width: 100}}
                source={{
                  uri: item.thumb,
                }}
              />
              <Body style={{paddingLeft: 10}}>
                <H3 numberOfLines={1}>{item.title}</H3>
                <Text note numberOfLines={3}>
                  {item.descrip}
                </Text>
                <Text style={{color: 'gray'}} note>
                  {item.time}
                </Text>
              </Body>
            </CardItem>
          </Card>
        )}
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
  },
});
