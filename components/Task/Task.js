import {Body, Button, Form, Input, Item, Label, Text} from 'native-base';
import React, {Component} from 'react';
import {FlatList, StyleSheet, View} from 'react-native';
import TaskDelete from './TaskDelete';

export default class Task extends Component {
  constructor(props) {
    super(props);

    this.state = {
      task: '',
      taskError: '',
      description: '',
      descriptionError: '',
      toDoTask: [
        {
          id: Math.random(),
          task: 'Lorem ipsum dolor sit amet consectetur adipisicing .',
          description:
            'Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorem velit sint eligendi facere cumque accusantium suscipit eveniet blanditiis, earum sit. ',
        },
      ],
      errorMsg: false,
    };
  }

  validate = () => {
    let taskError = '';
    let descriptionError = '';

    if (!this.state.task) {
      taskError = ' * Task Cannot Be Blank';
    }
    if (!this.state.description) {
      descriptionError = ' * Description Cannot Be Blank';
    }
    if (taskError || descriptionError) {
      this.setState({
        taskError,
        descriptionError,
      });
      return false;
    }
    return true;
  };

  _mySubmit = () => {
    let toDoTask = {
      id: Math.random(),
      task: this.state.task,
      description: this.state.description,
    };

    if (this.validate()) {
      this.state.toDoTask.push(toDoTask);
      this.setState({
        task: '',
        description: '',
        taskError: '',
        descriptionError: '',
      });
    } else {
      this.setState({
        errorMsg: true,
      });
    }
  };

  deleteItem = (index) => {
    const arr = [...this.state.toDoTask];
    arr.splice(index, 1);
    this.setState({
      toDoTask: arr,
    });
  };

  render() {
    return (
      <View style={styles.container}>
        <Form>
          <Item floatingLabel>
            <Label>Task</Label>
            <Input
              onChangeText={(e) => this.setState({task: e})}
              value={this.state.task}
            />
          </Item>
          {!this.state.task ? (
            <Text
              style={{
                display: this.state.errorMsg ? 'flex' : 'none',
                color: 'red',
              }}>
              {this.state.taskError}
            </Text>
          ) : null}

          <Item floatingLabel>
            <Label>Description</Label>
            <Input
              onChangeText={(e) => this.setState({description: e})}
              value={this.state.description}
            />
          </Item>
          {!this.state.description ? (
            <Text
              style={{
                display: this.state.errorMsg ? 'flex' : 'none',
                color: 'red',
              }}>
              {this.state.descriptionError}
            </Text>
          ) : null}
        </Form>
        <View style={{marginTop: '5%'}}>
          <Button full onPress={this._mySubmit}>
            <Text>Submit</Text>
          </Button>
        </View>
        {this.state.toDoTask < 1 ? null : (
          <FlatList
            keyExtractor={(i) => i.id.toString()}
            data={this.state.toDoTask}
            renderItem={({item}) => (
              <TaskDelete
                data={item}
                handleDelete={() => this.deleteItem(item.id)}
              />
            )}
          />
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
