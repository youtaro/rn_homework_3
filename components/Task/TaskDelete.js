import {Body, Button, Card, CardItem, Text} from 'native-base';
import React from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Animated,
  TouchableOpacity,
  Platform,
} from 'react-native';
import Swipeable from 'react-native-gesture-handler/Swipeable';
import Icon from 'react-native-vector-icons/Ionicons';

const SCREEN_WIDTH = Dimensions.get('window').width;

const TaskDelete = (props) => {
  const leftSwipe = (progress, dragX) => {
    const scale = dragX.interpolate({
      inputRange: [0, 100],
      outputRange: [0, 1],
      extrapolate: 'clamp',
    });
    return (
      <TouchableOpacity onPress={props.handleDelete} activeOpacity={0.6}>
        <View style={styles.deleteBox}>
          <Animated.Text>
            <Icon color="white" size={25} name="trash-bin-outline" />
          </Animated.Text>
        </View>
      </TouchableOpacity>
    );
  };
  return (
    <>
      <Swipeable renderRightActions={leftSwipe}>
        <Card>
          <CardItem header>
            <Text numberOfLines={1}>{props.data.task}</Text>
          </CardItem>
          <CardItem>
            <Body>
              <Text numberOfLines={1}>{props.data.description}</Text>
            </Body>
          </CardItem>
        </Card>
      </Swipeable>
    </>
  );
};

export default TaskDelete;

const styles = StyleSheet.create({
  container: {
    height: 80,
    width: SCREEN_WIDTH,
    backgroundColor: 'white',
    justifyContent: 'center',
    padding: 16,
  },
  deleteBox: {
    backgroundColor: 'red',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 5,
    width: 100,
    height: Platform.OS === 'ios' ? 90 : 103,
  },
  txtWhite: {
    color: 'white',
  },
});
