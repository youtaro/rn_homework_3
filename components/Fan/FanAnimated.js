import {ActionSheet, Button, Icon, Root} from 'native-base';
import React, {Component} from 'react';

import {
  StyleSheet,
  Image,
  View,
  Animated,
  TouchableWithoutFeedback,
  Easing,
  Text,
  ActionSheetIOS,
  Alert,
} from 'react-native';
import {_getFanColor, _getFanStand} from '../../Data/ColorAssets';

var BUTTONS = ['Default', 'Red', 'Blue', 'Cancel'];

export default class animations extends Component {
  state = {
    animation: new Animated.Value(0),
    stopStart: 0,
    colorCheck: 'black',
  };

  onPress = () => {
    ActionSheet.show(
      {
        options: BUTTONS,
        cancelButtonIndex: 3,
        destructiveButtonIndex: 3,
        title: 'Change Fan Color',
      },

      (buttonIndex) => {
        if (buttonIndex === 0) {
          this.setState({
            colorCheck: 'black',
          });
        } else if (buttonIndex === 1) {
          this.setState({
            colorCheck: 'red',
          });
        } else if (buttonIndex === 2) {
          this.setState({
            colorCheck: 'blue',
          });
        } else if (buttonIndex === 3) {
          this.setState({
            colorCheck: 'black',
          });
        }
      },
    );
  };
  _startFan = (timer) => {
    this.state.animation.setValue(0);
    Animated.loop(
      Animated.timing(this.state.animation, {
        toValue: 1,
        duration: 300,
        useNativeDriver: true,
        easing: Easing.linear,
      }),
    ).start();

    if (timer > 0) {
      setTimeout(() => {
        this._stopFan();
      }, timer * 60000);
    }
  };

  _setTimer = () => {
    Alert.prompt(
      'Set Timer',
      null,
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {
          text: 'OK',
          onPress: (timer) => this._startFan(timer),
        },
      ],
      'plain-text',
    );
  };

  _stopFan = () => {
    Animated.timing(this.state.animation).stop();
  };

  render() {
    const rotateInterPolate = this.state.animation.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '360deg'],
    });
    const animatedStyles = {
      transform: [{rotate: rotateInterPolate}],
    };

    return (
      <>
        <Root>
          <Icon
            onPress={this.onPress}
            name="build-outline"
            style={styles.setting}
          />
          <View style={styles.container}>
            <View
              style={{
                borderWidth: 2,
                borderRadius: 200,
                width: 230,
                height: 230,
                alignItems: 'center',
                justifyContent: 'center',
                borderColor: this.state.colorCheck,
              }}>
              <Animated.View style={animatedStyles}>
                <Image
                  style={styles.img}
                  source={_getFanColor(this.state.colorCheck)}
                />
              </Animated.View>
            </View>
            <View style={styles.row}>
              <Button style={styles.btn} onPress={() => this._startFan()}>
                <Text>Start</Text>
              </Button>
              <Button danger style={styles.btn} onPress={() => this._stopFan()}>
                <Text>Stop</Text>
              </Button>
              <Button
                warning
                style={styles.btn}
                onPress={() => this._setTimer()}>
                <Text>Set Timer</Text>
              </Button>
            </View>
          </View>
        </Root>
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  img: {width: 200, height: 200},
  setting: {
    alignSelf: 'flex-end',
    marginRight: 15,
    marginTop: 5,
    color: 'gray',
  },
  btn: {
    marginLeft: 2,
    padding: 10,
  },
  row: {
    flexDirection: 'row',
    marginTop: '3%',
    alignItems: 'center',
  },
});
