import {
  Body,
  Button,
  Footer,
  FooterTab,
  Header,
  Left,
  Right,
  Title,
  Text,
} from 'native-base';
import FanAnimated from './components/Fan/FanAnimated';
import React, {Component} from 'react';
import {Platform, StatusBar, StyleSheet} from 'react-native';
import Task from './components/Task/Task';
import Home from './components/Home/Home';
import Icon from 'react-native-vector-icons/Ionicons';

export default class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedTab: 'home',
    };
  }

  renderSelectedTab = () => {
    switch (this.state.selectedTab) {
      case 'home':
        return <Home />;
        break;
      case 'task':
        return <Task />;
        break;
      case 'fan':
        return <FanAnimated />;
        break;
      default:
    }
  };

  render() {
    return (
      <>
        <Header style={styles.bckBlue}>
          <StatusBar
            backgroundColor="#3482f7"
            barStyle={Platform.OS ? 'light-content' : 'default'}
          />
          <Left>
            <Button transparent>
              <Icon size={30} color="white" name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title style={styles.textWhite}>HOME RN 02</Title>
          </Body>
          <Right>
            <Button transparent>
              <Icon
                size={30}
                color="white"
                name="ellipsis-horizontal-circle-outline"
              />
            </Button>
          </Right>
        </Header>
        {this.renderSelectedTab()}
        <Footer style={styles.bckBlue}>
          <FooterTab style={styles.bckBlue}>
            <Button
              active={this.state.selectedTab === 'home'}
              onPress={() => this.setState({selectedTab: 'home'})}>
              <Icon size={30} color="white" name="home" />
              <Text style={styles.textWhite}>Home</Text>
            </Button>
            <Button
              active={this.state.selectedTab === 'task'}
              onPress={() => this.setState({selectedTab: 'task'})}>
              <Icon size={30} color="white" name="clipboard" />
              <Text style={styles.textWhite}>Task</Text>
            </Button>
            <Button
              active={this.state.selectedTab === 'fan'}
              onPress={() => this.setState({selectedTab: 'fan'})}>
              <Icon size={30} color="white" name="md-flower" />
              <Text style={styles.textWhite}>Fan</Text>
            </Button>
          </FooterTab>
        </Footer>
      </>
    );
  }
}

const styles = StyleSheet.create({
  textWhite: {color: 'white'},
  bckBlue: {
    backgroundColor: '#3482f7',
  },
});
